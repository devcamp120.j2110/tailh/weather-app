// Hook
import React, { useState, useEffect } from "react";
// Axios
import axios from "axios";
// Icon
import { BsSearch } from "react-icons/bs";

function App() {
  // State
  const [weatherData, setWeatherData] = useState({});
  const [location, setLocation] = useState("");
  const weatherApiURL = `https://api.openweathermap.org/data/2.5/weather?q=${location}&appid=681b2acd49630361803d924660a4938f`;
  // Handle change input
  const onChangeInputSearch = (event) => {
    setLocation(event.target.value);
  };
  // Handle Event Key
  const searchLocation = (event) => {
    // Press Enter --> Call API
    if (event.key === "Enter") {
      axios.get(weatherApiURL).then((res) => {
        console.log(res.data);
        setWeatherData(res.data);
        // reset state
        setLocation("");
      });
    }
    // Press Escape
    if (event.key === "Escape") {
      // reset state
      setLocation("");
    }
  };
  // Handle Click
  const onSearchButtonClick = () => {
    axios.get(weatherApiURL).then((res) => {
      console.log(res.data);
      setWeatherData(res.data);
      // reset state
      setLocation("");
    });
  };

  useEffect(() => {
    axios
      .get(
        `https://api.openweathermap.org/data/2.5/weather?q=${"ho chi minh"}&appid=681b2acd49630361803d924660a4938f`
      )
      .then((res) => {
        console.log(res.data);
        setWeatherData(res.data);
      });
  }, []);
  return (
    <div className="app">
      <div className="search-wrapper">
        <input
          type="text"
          className="search-input"
          onChange={onChangeInputSearch}
          value={location}
          placeholder="Enter Location"
          onKeyPress={searchLocation}
        />
        <div className="icon-search">
          <BsSearch
            onClick={onSearchButtonClick}
            style={{ display: "block", margin: "auto" }}
          />
        </div>
      </div>
      <section className="container">
        <div className="content-wrapper">
          <div className="content-title">
            {weatherData.weather ? (
              <h2>{weatherData.weather[0].main}</h2>
            ) : null}
            {weatherData.name ? <h1>{weatherData.name}</h1> : null}
          </div>
          <div className="content">
            {weatherData.main ? (
              <h2>{weatherData.main.temp.toFixed(0)}°F</h2>
            ) : null}
            <ul>
              <li>
                <h4>Feels</h4>
                {weatherData.main ? (
                  <h3 className="bold">
                    {weatherData.main.feels_like.toFixed(0)}°F
                  </h3>
                ) : null}
              </li>
              <li>
                <h4>Humidity</h4>
                {weatherData.main ? (
                  <h3 className="bold">{weatherData.main.humidity}%</h3>
                ) : null}
              </li>
              <li>
                <h4>Wind speed</h4>
                {weatherData.main ? (
                  <h3 className="bold">{weatherData.wind.speed}</h3>
                ) : null}
              </li>
            </ul>
          </div>
        </div>
      </section>
    </div>
  );
}

export default App;
